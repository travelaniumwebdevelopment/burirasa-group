<?php require_once 'functions.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="language" content="en-us">
    <meta name="description" content="Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. The 4-star Luxury Boutique Resort on the Top-Sestination of Thailand : Samui, Phangan">
    <meta name="keywords" content="RasaHospitality,Management,Development,Hotel,Resort,ThePrivilege,ThePrivilegeHotelEzra,BeachClub,Thailand,Fashionable,Design,Modern,Cool,Relax,Villa,PoolVilla,Private,Adultonly,Bungalow,Beach, Adults only">
    <meta name='robots' content='index, follow, all' />
    <meta name="distribution" content="global">
    <meta name="rating" content="general">
    <meta name="googlebot" content="1. yes">
    <meta property="og:image" content="./assets/elements/bg-samui.jpg" />
    <meta property="og:description" content="Exclusive Offers available on Official Site ONLY. Best Rates Guaranteed. Member Deal. The 4-star Luxury Boutique Resort on the Top-Sestination of Thailand : Samui, Phangan"/>
    <meta property="og:title" content="Buri Rasa Village | Luxury Boutique Resort"/>
    <title>Buri Rasa Village | Luxury Boutique Resort</title>

    <?php include('./inc/_head.php'); ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PCJ2CBR');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PCJ2CBR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div class="page-home">
        <?php include './inc/header.php'; ?>

        

        <main class="site-main">
            <div class="site-cover">
                <div class="d-none d-lg-block">
                    <div class="background">
                        <img src="./uploads/2021/01/burirasa_cover.jpg" width="2560" height="1111" alt="Buri Rasa" />
                    </div>

                    <div class="logo samui"><img src="./assets/elements/logo-group/new-logo-samui.png" width="92" height="80" alt="logo-samui"></div>
                    <div class="logo phangan"><img src="./assets/elements/logo-group/new-logo-phangan.png" width="92" height="80" alt="logo-samui"></div>
    
                    <div class="panel-detail samui">
                        <div class="content">
                            <h4 class="title">Buri Rasa Village Samui</h4>
                            <p>Luxury 4-Star Boutique Resort in Koh Samui</p>
                        </div>
                        <div class="bottom">
                            <a class="btn btn-explore" href="http://www.burirasa.com/samui/">
                                <span class="text">EXPLORE</span>
                                <span class="deco"></span>
                            </a>
                        </div>
                    </div>
    
                    <div class="panel-detail phangan">
                        <div class="content">
                            <h4 class="title">Buri Rasa Village Phangan</h4>
                            <p>Luxury 4-Star Boutique Resort in Koh Phangan</p>
                        </div>
                        <div class="bottom">
                            <a class="btn btn-explore" href="http://www.burirasa.com/phangan/">
                                <span class="text">EXPLORE</span>
                                <span class="deco"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-lg-none">
                <div class="cover-samui">
                    <div class="background">
                        <img src="./uploads/2021/01/cover-samui.jpg" alt="Samui" width="1280" height="1208">
                    </div>
                    <div class="logo"><img src="./assets/elements/logo-group/new-logo-samui.png" width="92" height="80" alt="logo-samui"></div>
                    <div class="panel-detail">
                        <div class="content">
                            <h4 class="title">BURI RASA VILLAGE SAMUI</h4>
                            <p>Luxury 4-Star Boutique Resort in Koh Samui</p>
                        </div>
                        <div class="bottom">
                            <a class="btn btn-explore" href="https://www.burirasa.com/samui/chaweng/">
                                <span class="text">EXPLORE</span>
                                <span class="deco"></span>
                            </a>
                        </div>
                    </div>
                </div>
    
                <div class="cover-phangan">
                    <div class="background">
                        <img src="./uploads/2021/01/cover-phangan.jpg" alt="Phangan" width="1280" height="1208">
                    </div>
                    <div class="logo"><img src="./assets/elements/logo-group/new-logo-phangan.png" width="92" height="80" alt="logo-samui"></div>
                    <div class="panel-detail">
                        <div class="content">
                            <h4 class="title">BURI RASA VILLAGE PHANGAN</h4>
                            <p>Luxury 4-Star Boutique Resort in Koh Phangan</p>
                        </div>
                        <div class="bottom">
                            <a class="btn btn-explore" href="https://www.burirasa.com/phangan/">
                                <span class="text">EXPLORE</span>
                                <span class="deco"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>




            <section class="bg-tree">
                <div class="container">
                    <div class="row justify-content-center pb-5">
                        <div class="col-12">
                        	<div class="social text-center">
                        	    <h2 class="pb-5 pt-5 mb-0">------ FOLLOW BURI RASA ON SOCIAL ------</h2>
                        	</div>
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <div class="ig-icon-logo">
                                <img src="./assets/elements/new-logo-ig-samui.png" width="171" height="171" alt="">
                                <span class="ig-title"><a target="_blank" href="https://www.instagram.com/burirasavillagesamui/">burirasavillagesamui</a></span>
                            </div>
                            <div class="swiper-container swiper-samui">
                                <div class="swiper-wrapper ig-feed-samui" data-instagram-feed=""></div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <div class="ig-icon-logo">
                                <img src="./assets/elements/new-logo-ig-phangan.png" width="171" height="171" alt="">
                                <span class="ig-title"><a target="_blank" href="https://www.instagram.com/burirasavillagephangan/">burirasavillagephangan</a></span>
                            </div>
                            <div class="swiper-container swiper-phangan">
                                <div class="swiper-wrapper ig-feed-phangan" data-instagram-feed=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <?php include 'inc/footer.php'; ?>
    </div>
</body>

<?php include 'inc/_footer.php'; ?>
</html>