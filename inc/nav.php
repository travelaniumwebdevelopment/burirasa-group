<li class="nav-home" itemprop="url"><a href="http://www.burirasa.com/samui" title="Buri Rasa Village Koh Samui">Home</a></li>
<li class="nav-room" itemprop="url"><a href="samui-deluxe-suite.php" title="Accommodation">Accommodation</a></li>
<li class="nav-dining" itemprop="url"><a href="samui-beach-dining.php" title="Dining">Dining</a></li>
<li class="nav-activities" itemprop="url"><a href="activities-in-samui.php" title="Activities">Activities</a></li>
<li class="nav-spa" itemprop="url"><a href="samui-herbal-spa.php" title="Nam Thai Herbal Spa">Nam Thai Spa</a></li>
<li class="nav-gallery" itemprop="url"><a href="samui-hotel-photos.php" title="Gallery">Gallery</a></li>
<li class="nav-360" itemprop="url"><a href="http://www.bangkokin360.com/virtualtour/burirasa" title="360 Tour" target="_blank">360 Tour</a></li>
<li class="nav-offers" itemprop="url"><a href="samui-hotel-offer.php" title="Offers">Offers</a></li>
<li class="nav-location" itemprop="url"><a href="samui-hotel-map-location.php" title="Location">Location</a></li>
<li class="nav-contact" itemprop="url"><a href="contact.php" title="Contact">Contact</a></li>
