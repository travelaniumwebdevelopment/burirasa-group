        <footer class="site-footer">
            <div class="container">
                <div class="row py-3">
                    <div class="col-12 col-lg-3 block py-3">
                        <img class="icon-contact" src="assets/elements/logo-rasa.png" width="25" height="50" alt="logo"></img>
                        <div class="content">
                            <span class="title">MANAGED BY<br></span>
                            <span class="detail"><a href="http://www.rasahospitality.com/" target="_blank">Rasa Hospitality</a></span>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3 block py-3">
                        <div class="d-flex align-items-top">
                            <i class="icon-contact rotate fas fa-phone" aria-hidden="true"></i>
                            <div class="content">
                                <span class="title">TELEPHONE<br></span>
                                <span class="detail"><a href="tel:+6629370181">(+66) 0 2937 0181</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3 block py-3">
                        <div class="d-flex align-items-top">
                            <i class="icon-contact far fa-envelope" aria-hidden="true"></i>
                            <div class="content">
                                <span class="title">EMAIL<br></span>
                                <span class="detail"><a href="mailto:info@rasahospitality.com">info@rasahospitality.com</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3 block py-3">
                        <div class="d-flex align-items-top">
                            <i class="icon-contact fas fa-map-marker-alt" aria-hidden="true"></i>
                            <div class="content">
                                <span class="title">ADDRESS<br></span>
                                <span class="detail"><a target="_blank" href="https://goo.gl/maps/oVxVPCJKEtL2">Rasa Hospitality Management &amp; Development Rasa Tower 27th Fl, 555 Phaholyothin Rd, Chatuchak, Bangkok 10900 Thailand</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <p class="tl-m"><i class="fal fa-copyright" aria-hidden="true"></i> Copyright 2018 Buri Rasa Village Group. All rights reserved.</p>
                        </div>            
                        <div class="col-12 col-md-6">
                            <p class="tr-m mb-md-0">Hotel Web Design by <a href="https://www.travelanium.com" target="_blank" title="travelanium">Travelanium</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="drag-panel"></div>
            <div id="back-top"><a href="#page" class="arrows bounce"><i class="fas fa-chevron-up" aria-hidden="true"></i></a></div>
        </footer>