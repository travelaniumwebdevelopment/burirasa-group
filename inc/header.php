        <header class="site-header">
            <div class="top-bar">
                <div class="row no-gutters">
                    <div class="col-12 col-md-8 desktop">
                        <ul class="d-flex justify-content-start align-items-center ml-custom">
                            <li><a class="active" href="./">Group</a></li>
                            <li><a target="_blank" href="http://www.burirasa.com/samui/">Koh Samui</a></li>
                            <li><a target="_blank" href="http://www.burirasa.com/phangan/">Koh Phangan</a></li>
                            <!-- <li><a target="_blank" href="http://www.burirasa.com/chiangmai/">Chiang mai</a></li> -->
                        </ul>
                    </div>
                    <div class="col-12 col-md-4">
                        <ul class="d-flex justify-content-end align-items-center">
                            <li><a target="_blank" href="http://www.rasahospitality.com/"><i class="fas fa-map-marker-alt" aria-hidden="true"></i> Partner Hotels Rasa Group</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="main-bar">
                <div class="container">
                    <div id="main"><span onclick="openNav()"><img src="./assets/elements/icon-hamburger-menu.png" width="30" height="23" alt="icon-hamburger"></span></div>
                    <div class="site-branding">
                        <a href="./"><img class="logo" src="./assets/elements/logo.png" width="85" height="55" alt="img-logo"></a>
                    </div>
                    <nav class="site-booking-bar">
                        <form id="booking_panel" class="booking-bar">
                            <div class="form-group">
                                <label for="selectproperty" class="text-uppercase mb-0" style="top:2px;">SELECT</label>
                                <select name="propertyId" class="form-control custom-select input-select select-property" required style="width:107px;">
                                    <?php include('inc/property-option.php'); ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <a class="input-display-group form-control input-text custom-select datepicker clickable collapsed" id="display_guest_room" href="#collapse_datepicker" data-toggle="collapse">
                                    <label for="checkin-checkout" class="text-uppercase mb-0">ARRIVE-DEPART</label>
                                    <div class="input-display">
                                        <span data-display-arrive>Arrive</span> - <span data-display-depart>Depart</span>
                                    </div>
                                </a>

                                <div id="collapse_datepicker" class="collapse-solo collapse-panel collapse">
                                    <div class="inner">
                                        <div class="content">
                                            <div id="datepicker_panel">
                                                <div class="arrive panel">
                                                    <div id="arrive_label" class="label">Check-in <span class="label-date" data-display-checkin></span></div>
                                                    <div id="arrive_datepicker" data-datepicker-checkin></div>
                                                </div>
                                                <div class="depart panel">
                                                    <div id="depart_label" class="label">Check-out <span class="label-date" data-display-checkout></span></div>
                                                    <div id="depart_datepicker" data-datepicker-checkout></div>
                                                </div>
                                                <div class="diff" data-display-diff></div>
                                            </div>
                                            <button class="btn btn-light btn-block" id="update_datepicker" data-datepicker-confirm>Update</button>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="checkin" value="">
                                <input type="hidden" name="checkout" value="">
                            </div>
                            <div class="form-group">
                                <a class="input-display-group form-control input-select custom-select clickable collapsed" id="display_guest_room" href="#collapse_guest" data-toggle="collapse">
                                    <label for="checkin-checkout" class="text-uppercase mb-0">GUESTS</label>
                                    <div class="input-display">
                                        <span id="num_guest" data-display-guest>2 Guests</span> / <span id="num_room" data-display-room>1 Room</span>
                                    </div>
                                </a>

                                <div id="collapse_guest" class="collapse-solo collapse-panel collapse">
                                    <div class="inner">
                                        <div class="content">
                                            <div id="adult_counter" class="counter">
                                                <div class="counter-label">Adult(s)</div>
                                                <div class="counter-control" data-counter data-counter-min="1" data-counter-max="12">
                                                    <span class="counter-item counter-val" data-value>1</span>
                                                    <span class="counter-item counter-inc" tabindex="0" data-increment><i class="far fa-plus"></i></span>
                                                    <span class="counter-item counter-dec" tabindex="0" data-decrement><i class="far fa-minus"></i></span>
                                                </div>
                                            </div>

                                            <div id="child_counter" class="counter">
                                                <div class="counter-label">Child(ren)</div>
                                                <div class="counter-control" data-counter data-counter-min="0" data-counter-max="5">
                                                    <span class="counter-item counter-val" data-value>0</span>
                                                    <span class="counter-item counter-inc" tabindex="0" data-increment><i class="far fa-plus"></i></span>
                                                    <span class="counter-item counter-dec" tabindex="0" data-decrement><i class="far fa-minus"></i></span>
                                                </div>
                                            </div>

                                            <div id="room_counter" class="counter">
                                                <div class="counter-label">Room(s)</div>
                                                <div class="counter-control" data-counter data-counter-min="1" data-counter-max="9">
                                                    <span class="counter-item counter-val" data-value>1</span>
                                                    <span class="counter-item counter-inc" tabindex="0" data-increment><i class="far fa-plus"></i></span>
                                                    <span class="counter-item counter-dec" tabindex="0" data-decrement><i class="far fa-minus"></i></span>
                                                </div>
                                            </div>

                                            <button class="btn btn-light btn-block mt-3" id="update_guest" data-counter-confirm>Update</button>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="numofroom" value="1">
                                <input type="hidden" name="numofadult" value="2">
                                <input type="hidden" name="numofchild" value="0">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control input-text" name="accesscode" placeholder="Promo Code" style="width:95px; padding: .5rem .75rem .5rem .75rem; margin-top: 5px;">
                            </div>
                            <button type="submit" class="btn-submit">Book</button>
                        </form>
                        <div id="member" class="desktop-btn-member btn-action">
                            <div class="member-welcome">
                                <div class="container-fluid">
                                    <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="24" height="29">
                                        <path fill-rule="evenodd" fill="#fff" d="M21.979,6.708 C21.132,7.328 20.071,7.877 18.887,8.313 C19.005,8.782 19.022,9.281 18.940,9.770 L16.079,26.878 C15.848,28.257 14.563,29.192 13.210,28.964 L2.054,27.082 C0.702,26.853 -0.209,25.548 0.021,24.169 L2.883,7.062 C3.044,6.101 3.579,5.246 4.367,4.694 L10.310,0.504 C11.523,-0.352 13.181,-0.072 14.049,1.135 L14.064,1.155 C14.402,1.049 14.751,0.956 15.110,0.871 C17.059,0.412 18.978,0.306 20.533,0.568 C20.562,0.573 20.585,0.577 20.614,0.582 C23.142,1.032 23.785,2.304 23.944,3.022 C24.103,3.740 24.054,5.173 21.979,6.708 ZM11.421,22.318 C12.589,22.515 13.705,21.703 13.904,20.513 C14.104,19.322 13.312,18.189 12.145,17.992 C10.977,17.795 9.861,18.607 9.662,19.797 C9.463,20.987 10.254,22.121 11.421,22.318 ZM5.528,21.166 C5.717,21.386 6.046,21.411 6.268,21.218 L12.985,15.316 C13.201,15.122 13.228,14.787 13.040,14.562 C12.851,14.342 12.522,14.317 12.299,14.510 L5.583,20.412 C5.366,20.605 5.340,20.940 5.528,21.166 ZM7.154,13.399 C5.986,13.202 4.870,14.014 4.671,15.204 C4.477,16.401 5.263,17.528 6.431,17.725 C7.598,17.922 8.714,17.110 8.914,15.920 C9.113,14.730 8.322,13.596 7.154,13.399 ZM20.245,2.683 C20.227,2.680 20.204,2.676 20.187,2.673 C18.915,2.458 17.241,2.558 15.576,2.949 C15.514,2.969 15.452,2.983 15.390,2.997 L17.096,5.375 C16.757,5.482 16.402,5.579 16.025,5.667 C14.955,5.917 13.888,6.046 12.918,6.052 C12.633,5.483 12.098,5.053 11.428,4.940 C10.306,4.751 9.244,5.523 9.053,6.666 C8.862,7.810 9.614,8.888 10.736,9.077 C11.857,9.266 12.919,8.494 13.111,7.351 C13.119,7.304 13.120,7.262 13.127,7.214 C14.147,7.229 15.293,7.095 16.431,6.826 C20.322,5.913 22.026,4.080 21.903,3.502 C21.863,3.313 21.462,2.900 20.245,2.683 ZM7.311,16.298 C6.912,16.588 6.351,16.493 6.068,16.088 C5.786,15.683 5.881,15.112 6.280,14.821 C6.679,14.531 7.240,14.626 7.523,15.031 C7.805,15.442 7.710,16.008 7.311,16.298 ZM11.265,19.413 C11.665,19.117 12.226,19.211 12.508,19.623 C12.791,20.028 12.695,20.600 12.296,20.890 C11.897,21.180 11.336,21.085 11.053,20.680 C10.771,20.275 10.866,19.703 11.265,19.413 Z"/>
                                    </svg>
                                    <div class="text-member">
                                        <span>HI! MEMBER</span>
                                        <span class="member-note">Now you can access our secret offers</span>
                                    </div>
                                </div>
                            </div>
                            <a id="member_price" class="btn-collapse-toggle member-form-toggle custom-toggle" data-toggle="collapse" href="#collapseMember" aria-expanded="false" aria-controls="collapseMember">
                                <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="24" height="29">
                                    <path fill-rule="evenodd" fill="#fff" d="M21.979,6.708 C21.132,7.328 20.071,7.877 18.887,8.313 C19.005,8.782 19.022,9.281 18.940,9.770 L16.079,26.878 C15.848,28.257 14.563,29.192 13.210,28.964 L2.054,27.082 C0.702,26.853 -0.209,25.548 0.021,24.169 L2.883,7.062 C3.044,6.101 3.579,5.246 4.367,4.694 L10.310,0.504 C11.523,-0.352 13.181,-0.072 14.049,1.135 L14.064,1.155 C14.402,1.049 14.751,0.956 15.110,0.871 C17.059,0.412 18.978,0.306 20.533,0.568 C20.562,0.573 20.585,0.577 20.614,0.582 C23.142,1.032 23.785,2.304 23.944,3.022 C24.103,3.740 24.054,5.173 21.979,6.708 ZM11.421,22.318 C12.589,22.515 13.705,21.703 13.904,20.513 C14.104,19.322 13.312,18.189 12.145,17.992 C10.977,17.795 9.861,18.607 9.662,19.797 C9.463,20.987 10.254,22.121 11.421,22.318 ZM5.528,21.166 C5.717,21.386 6.046,21.411 6.268,21.218 L12.985,15.316 C13.201,15.122 13.228,14.787 13.040,14.562 C12.851,14.342 12.522,14.317 12.299,14.510 L5.583,20.412 C5.366,20.605 5.340,20.940 5.528,21.166 ZM7.154,13.399 C5.986,13.202 4.870,14.014 4.671,15.204 C4.477,16.401 5.263,17.528 6.431,17.725 C7.598,17.922 8.714,17.110 8.914,15.920 C9.113,14.730 8.322,13.596 7.154,13.399 ZM20.245,2.683 C20.227,2.680 20.204,2.676 20.187,2.673 C18.915,2.458 17.241,2.558 15.576,2.949 C15.514,2.969 15.452,2.983 15.390,2.997 L17.096,5.375 C16.757,5.482 16.402,5.579 16.025,5.667 C14.955,5.917 13.888,6.046 12.918,6.052 C12.633,5.483 12.098,5.053 11.428,4.940 C10.306,4.751 9.244,5.523 9.053,6.666 C8.862,7.810 9.614,8.888 10.736,9.077 C11.857,9.266 12.919,8.494 13.111,7.351 C13.119,7.304 13.120,7.262 13.127,7.214 C14.147,7.229 15.293,7.095 16.431,6.826 C20.322,5.913 22.026,4.080 21.903,3.502 C21.863,3.313 21.462,2.900 20.245,2.683 ZM7.311,16.298 C6.912,16.588 6.351,16.493 6.068,16.088 C5.786,15.683 5.881,15.112 6.280,14.821 C6.679,14.531 7.240,14.626 7.523,15.031 C7.805,15.442 7.710,16.008 7.311,16.298 ZM11.265,19.413 C11.665,19.117 12.226,19.211 12.508,19.623 C12.791,20.028 12.695,20.600 12.296,20.890 C11.897,21.180 11.336,21.085 11.053,20.680 C10.771,20.275 10.866,19.703 11.265,19.413 Z"/>
                                </svg>
                                <div class="text-member">
                                    <span>SIGN IN <i class="fas fa-caret-down" aria-hidden="true"></i></span>
                                    <span class="member-note">For Exclusive Offers</span>
                                </div>
                            </a>
                            <div class="panel-member-price">
                                <div class="collapse" id="collapseMember">
                                    <div class="card card-body">
                                        <div class="form-member-price">
                                            <p><small>Not a member yet?<br>Get a minimum <span style="color:#e6c820;">10% OFF</span><br>the lowest rate!</small></p>
                                            <form class="form-member" id="form_member_desktop" action="/" target="_blank">
                                                <div class="form-group mb-0">
                                                    <input type="email" name="email" class="form-control" id="member_email" placeholder="Your email address" required />
                                                    <button class="btn-submit" type="submit">SIGN UP</button>
                                                    <a href="#collapseMember" class="btn-close" data-toggle="collapse">No, Thanks</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <div id="member" class="mobile-btn-member">
            <div class="member-welcome">
                <div class="container-fluid">
                    <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="24" height="29">
                        <path fill-rule="evenodd" fill="#fff" d="M21.979,6.708 C21.132,7.328 20.071,7.877 18.887,8.313 C19.005,8.782 19.022,9.281 18.940,9.770 L16.079,26.878 C15.848,28.257 14.563,29.192 13.210,28.964 L2.054,27.082 C0.702,26.853 -0.209,25.548 0.021,24.169 L2.883,7.062 C3.044,6.101 3.579,5.246 4.367,4.694 L10.310,0.504 C11.523,-0.352 13.181,-0.072 14.049,1.135 L14.064,1.155 C14.402,1.049 14.751,0.956 15.110,0.871 C17.059,0.412 18.978,0.306 20.533,0.568 C20.562,0.573 20.585,0.577 20.614,0.582 C23.142,1.032 23.785,2.304 23.944,3.022 C24.103,3.740 24.054,5.173 21.979,6.708 ZM11.421,22.318 C12.589,22.515 13.705,21.703 13.904,20.513 C14.104,19.322 13.312,18.189 12.145,17.992 C10.977,17.795 9.861,18.607 9.662,19.797 C9.463,20.987 10.254,22.121 11.421,22.318 ZM5.528,21.166 C5.717,21.386 6.046,21.411 6.268,21.218 L12.985,15.316 C13.201,15.122 13.228,14.787 13.040,14.562 C12.851,14.342 12.522,14.317 12.299,14.510 L5.583,20.412 C5.366,20.605 5.340,20.940 5.528,21.166 ZM7.154,13.399 C5.986,13.202 4.870,14.014 4.671,15.204 C4.477,16.401 5.263,17.528 6.431,17.725 C7.598,17.922 8.714,17.110 8.914,15.920 C9.113,14.730 8.322,13.596 7.154,13.399 ZM20.245,2.683 C20.227,2.680 20.204,2.676 20.187,2.673 C18.915,2.458 17.241,2.558 15.576,2.949 C15.514,2.969 15.452,2.983 15.390,2.997 L17.096,5.375 C16.757,5.482 16.402,5.579 16.025,5.667 C14.955,5.917 13.888,6.046 12.918,6.052 C12.633,5.483 12.098,5.053 11.428,4.940 C10.306,4.751 9.244,5.523 9.053,6.666 C8.862,7.810 9.614,8.888 10.736,9.077 C11.857,9.266 12.919,8.494 13.111,7.351 C13.119,7.304 13.120,7.262 13.127,7.214 C14.147,7.229 15.293,7.095 16.431,6.826 C20.322,5.913 22.026,4.080 21.903,3.502 C21.863,3.313 21.462,2.900 20.245,2.683 ZM7.311,16.298 C6.912,16.588 6.351,16.493 6.068,16.088 C5.786,15.683 5.881,15.112 6.280,14.821 C6.679,14.531 7.240,14.626 7.523,15.031 C7.805,15.442 7.710,16.008 7.311,16.298 ZM11.265,19.413 C11.665,19.117 12.226,19.211 12.508,19.623 C12.791,20.028 12.695,20.600 12.296,20.890 C11.897,21.180 11.336,21.085 11.053,20.680 C10.771,20.275 10.866,19.703 11.265,19.413 Z"/>
                    </svg>
                    <div class="text-member">
                        <span>HI! MEMBER</span><br>
                        <span class="member-note">Now you can access our secret offers</span>
                    </div>
                </div>
            </div>
            <a class="member-form-toggle" data-toggle="collapse" href="#collapseMemberMobile" aria-expanded="false" aria-controls="collapseMemberMobile">
                <div class="container-fluid">
                    <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="24" height="29">
                        <path fill-rule="evenodd" fill="#fff" d="M21.979,6.708 C21.132,7.328 20.071,7.877 18.887,8.313 C19.005,8.782 19.022,9.281 18.940,9.770 L16.079,26.878 C15.848,28.257 14.563,29.192 13.210,28.964 L2.054,27.082 C0.702,26.853 -0.209,25.548 0.021,24.169 L2.883,7.062 C3.044,6.101 3.579,5.246 4.367,4.694 L10.310,0.504 C11.523,-0.352 13.181,-0.072 14.049,1.135 L14.064,1.155 C14.402,1.049 14.751,0.956 15.110,0.871 C17.059,0.412 18.978,0.306 20.533,0.568 C20.562,0.573 20.585,0.577 20.614,0.582 C23.142,1.032 23.785,2.304 23.944,3.022 C24.103,3.740 24.054,5.173 21.979,6.708 ZM11.421,22.318 C12.589,22.515 13.705,21.703 13.904,20.513 C14.104,19.322 13.312,18.189 12.145,17.992 C10.977,17.795 9.861,18.607 9.662,19.797 C9.463,20.987 10.254,22.121 11.421,22.318 ZM5.528,21.166 C5.717,21.386 6.046,21.411 6.268,21.218 L12.985,15.316 C13.201,15.122 13.228,14.787 13.040,14.562 C12.851,14.342 12.522,14.317 12.299,14.510 L5.583,20.412 C5.366,20.605 5.340,20.940 5.528,21.166 ZM7.154,13.399 C5.986,13.202 4.870,14.014 4.671,15.204 C4.477,16.401 5.263,17.528 6.431,17.725 C7.598,17.922 8.714,17.110 8.914,15.920 C9.113,14.730 8.322,13.596 7.154,13.399 ZM20.245,2.683 C20.227,2.680 20.204,2.676 20.187,2.673 C18.915,2.458 17.241,2.558 15.576,2.949 C15.514,2.969 15.452,2.983 15.390,2.997 L17.096,5.375 C16.757,5.482 16.402,5.579 16.025,5.667 C14.955,5.917 13.888,6.046 12.918,6.052 C12.633,5.483 12.098,5.053 11.428,4.940 C10.306,4.751 9.244,5.523 9.053,6.666 C8.862,7.810 9.614,8.888 10.736,9.077 C11.857,9.266 12.919,8.494 13.111,7.351 C13.119,7.304 13.120,7.262 13.127,7.214 C14.147,7.229 15.293,7.095 16.431,6.826 C20.322,5.913 22.026,4.080 21.903,3.502 C21.863,3.313 21.462,2.900 20.245,2.683 ZM7.311,16.298 C6.912,16.588 6.351,16.493 6.068,16.088 C5.786,15.683 5.881,15.112 6.280,14.821 C6.679,14.531 7.240,14.626 7.523,15.031 C7.805,15.442 7.710,16.008 7.311,16.298 ZM11.265,19.413 C11.665,19.117 12.226,19.211 12.508,19.623 C12.791,20.028 12.695,20.600 12.296,20.890 C11.897,21.180 11.336,21.085 11.053,20.680 C10.771,20.275 10.866,19.703 11.265,19.413 Z"/>
                    </svg>
                    <div class="text-member">
                        <span>SIGN IN <i class="fas fa-caret-down" aria-hidden="true"></i></span><br>
                        <span class="member-note">For Exclusive Offers</span>
                    </div>
                </div>
            </a>
            <div class="panel-member-price">
                <div class="collapse" id="collapseMemberMobile">
                    <div class="panel-member-price-wrapper">
                        <div class="form-member-price">
                            <p><small>Not a member yet?<br>Get a minimum <span style="color:#e6c820;">10% OFF</span> the lowest rate!</small></p>
                            <form class="form-member" id="form_member_mobile" action="/" target="_blank">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" id="member_email" placeholder="Your email address" required />
                                    <button class="btn btn-primary btn-submit" type="submit">SIGN UP</button>
                                    <a href="#collapseMemberMobile" class="btn btn-close ml-2" data-toggle="collapse">No, Thanks</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-menu">
            <div class="m-panel-background"></div>
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <div class="slide-menu-here">
                    <ul class="menu-bar">
                        <li><a href="./"><img class="logo" src="./assets/elements/logo-color.png" width="80" height="100" alt="logo"></a></li>
                        <li><a target="_blank" class="hover-menu" href="http://www.burirasa.com/samui/">Koh Samui</a></li>
                        <li><a target="_blank" class="hover-menu" href="http://www.burirasa.com/phangan/">Koh Phangan</a></li>
                        <!-- <li><a target="_blank" class="hover-menu" href="http://www.burirasa.com/chiangmai/">Chiang mai</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="mobile-booking">
            <form id="mobile_form" class="d-flex" action="/" target="_blank">
                <input type="hidden" name="onlineId" value="5">
                <input type="hidden" name="accesscode" value="">
                <select name="propertyId" class="form-control input-select custom-select" required>
                    <?php include('inc/property-option.php'); ?>
                </select>
                <button type="submit" class="btn-submit">BOOK NOW</button>
            </form>
        </div>
        <div id="member_modal" class="zoom-anim-dialog mfp-hide">
            <h3>Hi Member!!</h3>
            <p>Now you can access our secret offers</p>
            <form id="member_popup_form" class="d-flex" action="/" target="_blank">
                <input type="hidden" name="onlineId" value="5">
                <input type="hidden" name="accesscode" value="">
                <select name="propertyId" class="form-control input-select custom-select" required>
                    <?php include('inc/property-option.php'); ?>
                </select>
                <button type="submit" class="btn-submit">BOOK NOW</button>
            </form>
        </div>
         