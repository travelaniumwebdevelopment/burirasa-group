;(function($, window) {
    $.fn.instagramGallerySamui = function(options) {
        var defaults = {
            token: null,
            limit: 9,
            complete: function(){},
        };
        var configs = $.extend('', defaults, options);

        return this.each(function() {
            var $el = $(this);
            var path = window.location.origin;
            var subpath = (window.location.hostname === 'localhost' || window.location.hostname === '127.0.0.1') ? '/burirasa-group' : '';
            var data = $.getJSON(path + subpath + '/apps/instagram-samui.php');
            data.then(function(response) {
                var images  = response.data;
                var limit   = Math.min(configs.limit, images.length);

                for (var i = 0; i <= limit-1; i++) {
                    var img     = images[i];
                    var caption = (img.caption) ? img.caption.text : '';
                    var url = img.link;
                    var thumb   = {
                        src: img.images.thumbnail.url,
                        width: img.images.thumbnail.width,
                        height: img.images.thumbnail.height,
                    };
                    var large   = {
                        src: img.images.standard_resolution.url,
                        width: img.images.standard_resolution.width,
                        height: img.images.standard_resolution.height,
                    };
                    var markup = '<div class="swiper-slide box-ratio-square"><a target="_blank" href="'+url+'"><img class="img-cover box-ratio-item" src="'+large.src+'" width="'+large.width+'" height="'+large.height+'" alt=""></a></div>';
                    $el.append(markup);
                }
            });
            
            data.done(function() {
                configs.complete();
            });
        });
    }

})(jQuery, window);
