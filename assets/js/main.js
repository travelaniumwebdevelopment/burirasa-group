(function($){
    objectFitImages();
    objectFitVideos();
    
    $(window).on('scroll',function(){
        var pos = window.scrollY;
        if(pos >= 100){
            $('.site-header').addClass('mini');
        }else {
            $('.site-header').removeClass('mini');
        }
    });
    
    var $doc = $(document),
        $win = $(window);
    
    var winY = window.scrollY;
    $win.on('scroll', (function() {
        var scroll = window.scrollY;
        if (scroll > winY) {
            $('html')
                .addClass('scroll-down')
                .removeClass('scroll-up');
        } else {
            $('html')
                .addClass('scroll-up')
                .removeClass('scroll-down');
        }
        winY = scroll;
    }));

    $('#back-top').hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
            
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });
    
    // var owlsocial = $(".social-carousel");
    // owlsocial.owlCarousel({
    //     items: 1,
    //     nav: false,
    //     dots: false,
    //     loop: true,
    //     autoplay: true,
    //     autoplaySpeed: 1000,
    //     navText: ["<i class='fal fa-chevron-left' aria-hidden='true'></i>", "<i class='fal fa-chevron-right' aria-hidden='true'></i>"],
    // });

    //Simple Toggle Class menu
    $.fn.simpleToggleClass = function(toggleClassName, toggleClassElements, outfocus, behavior) {
        var $this = $(this);
        var className = toggleClassName;
        var elements = toggleClassElements;
        var outfocus = outfocus || false;
        var behavior = behavior || false;

        $this.on('click', function(e) {
            if( behavior === true )
                e.preventDefault();

            if( $this.hasClass(className) ) {
                $this.removeClass(className);
                $(elements).removeClass(className);
            } else {
                $this.addClass(className);
                $(elements).addClass(className);
            }
        });

        if( typeof outfocus === 'boolean' && outfocus === true ) {
            $('html').on('click', function(e) {
                var target = e.target;
                if( $(target).closest($this).length === 0 ) {
                    $this.removeClass(className);
                    $(elements).removeClass(className);
                }
            });
        }

        if( typeof outfocus === 'string' ) {
            $('html').on('click', function(e) {
                var target = e.target;
                var $outFocus = $(outfocus);
                if( $(target).closest($this).length === 0 && $(target).closest($outFocus).length === 0 ) {
                    $this.removeClass(className);
                    $(elements).removeClass(className);
                }
            });
        }
    }

    /*** drag-panel swipe*/
    $('#drag-panel').swipe({
        swipeRight: function() {
            openNav();
            $('body').addClass('m-panel-visible');
        },
    });

    $('#mySidenav').swipe({
        swipeLeft: function() {
            closeNav();
            $('body').removeClass('m-panel-visible');
        },
    });

    // Toggle Mobile Menu
    $('#main').simpleToggleClass('m-panel-visible', 'body', false, false);
    $('.m-panel-background').on('touchstart click', function() {
        $('body').removeClass('m-panel-visible');
        $('#main').removeClass('m-panel-visible');
        closeNav();
    });
    $('.closebtn').on('touchstart click', function() {
        $('body').removeClass('m-panel-visible');
        $('#main').removeClass('m-panel-visible');
        closeNav();
    });

    window.onresize = function() {
        if( window.innerWidth > 1200 ) {
            $('body').removeClass('m-panel-visible');
        }
    };
    
    $('#booking_panel, #mobile_form').booking({
        propertyId: '[name="propertyId"]',
        checkInSelector: '[name="checkin"]',
        checkOutSelector: '[name="checkout"]',
        adultSelector: '[name="numofadult"]',
        childSelector: '[name="numofchild"]',
        roomSelector: '[name="numofroom"]',
        codeSelector: '[name="accesscode"]',
        dateFormat: "dd M yy",
        secretCode: '',
        onlineId: 5,
    });

    //
    // Member
    //
    var memberCode = 'member';

    if (memberChecker()) {
        createMemberUI(); 
    } else {
        createGuestUI();
    }

    $('#form_member_desktop, #form_member_mobile').on('submit', function(event) {
        event.preventDefault();
        var pid = $(this).find('[name="propertyId"]').val();
        var email = $(this).find('[name="email"]').val();

        memberSendData({
            formAPI:    '45b2b804ac1200050152d978a5c5a2b8',
            formType:   2,
            customer: {
                email: email,
            }
        });

        memberCreateCookies({
            code: 'member',
            expire: 3,
            user: {
                email: email,
            }
        });

        $.magnificPopup.open({
            items: {
                type: 'inline',
                src: '#member_modal',
            },
    
            fixedContentPos: false,
            fixedBgPos: true,
    
            overflowY: 'auto',
    
            closeBtnInside: true,
            preloader: false,
            
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in',
            callbacks: {
                open: function() {
                    $('#member_popup_form').booking({
                        propertyId: '[name="propertyId"]',
                        secretCode: 'member',
                        onlineId: 5,
                    });
                },
            },
        });
        // var url = 'https://reservation.travelanium.net/propertyibe2/?propertyId='+ pid +'&onlineId=5&accesscode=member';
        // window.open(url);
        
        createMemberUI();
        
        return false;
    });

    function createMemberUI() {
        memberAppendCode('member');
        $('.mobile-btn-member, .desktop-btn-member').addClass('is-member').removeClass('is-guest');
        $('#collapseMember, #collapseMemberMobile').hide();
        $('#booking_panel, #mobile_form').booking('update', 'secretCode', memberCode);
    }

    function createGuestUI() {
        $('.mobile-btn-member, .desktop-btn-member').addClass('is-guest').removeClass('is-member');
    }

    // Video Playback
    // var $container = $('.video-container');
    // var $vid = $('#intro_video').get(0);

    // $container.on('click', function(e) {
    //     e.preventDefault();
    //     if( $vid.paused ) {
    //         $vid.play();
    //         $vid.volume=1;
    //         $container.removeClass('paused');
    //     } else {
    //         $vid.pause();
    //         $vid.volume=0;
    //         $container.addClass('paused');
    //     }
    // });

    $("body").on("contextmenu", "img", function(e) {
        return false;
    });

    // Instagram
    $('.ig-feed-samui').each(function() {
        $(this).instagramGallerySamui({
            limit: 12,
            complete: function($el) {
                new Swiper('.swiper-samui', {
                    effect: 'fade',
                    speed: 800,
                    loop: true,
                    preloadImages: true,
                    updateOnImagesReady: true,
                    autoplay: {
                        delay: 5000,
                        disableOnInteraction: false,
                    },
                });
            }
        });
    });
    $('.ig-feed-phangan').each(function() {
        $(this).instagramGalleryPhangan({
            limit: 12,
            complete: function($el) {
                new Swiper('.swiper-phangan', {
                    effect: 'fade',
                    speed: 800,
                    loop: true,
                    preloadImages: true,
                    updateOnImagesReady: true,
                    autoplay: {
                        delay: 5000,
                        disableOnInteraction: false,
                    },
                });
            }
        });
    });

})(jQuery);