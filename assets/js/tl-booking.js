"use strict";

var _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(e) {
    return typeof e;
} : function(e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e;
};

!function(e) {
    function t(t) {
        var n = t[i.name], o = !1, r = e(t).find(n.codeSelector).val(), a = n.secretCode;
        return "" !== r ? o = r : null !== a && (o = a), o;
    }
    function n(n, r, a) {
        var c, l, u = n, d = n[i.name], f = e(n), m = {};
        e.isNumeric(d.propertyId) ? l = d.propertyId : f.find(d.propertyId).length ? l = f.find(d.propertyId).val() : console.error("Property ID is not set or invalid format"), 
        o(m, "propertyId", l), o(m, "onlineId", d.onlineId), o(m, "checkin", r.val()), o(m, "checkout", a.val()), 
        f.find(d.adultSelector).length && o(m, "numofadult", f.find(d.adultSelector).val()), 
        f.find(d.childSelector).length && o(m, "numofchild", f.find(d.childSelector).val()), 
        f.find(d.roomSelector).length && o(m, "numofroom", f.find(d.roomSelector).val()), 
        t(u) && o(m, "accesscode", t(u)), null !== d.propertyGroup && o(m, "pgroup", d.propertyGroup), 
        null !== d.currency && o(m, "currency", d.currency), null !== d.confl && o(m, "confl", d.confl), 
        null !== d.language && o(m, "lang", d.language);
        if (c = "https://reservation.travelanium.net/propertyibe2?" + e.param(m), d.debug) return console.info(c), 
        !1;
        window.open(function(e, t) {
            var n = e;
            try {
                if (!ga) return n;
                ga(function(t) {
                    if (void 0 == t && (t = ga.getAll()[0]), !t) return n;
                    if (!t.get("linkerParam")) return n;
                    var o = new window.gaplugins.Linker(t);
                    n = o.decorate(e);
                });
            } catch (e) {
                console.info(e.message);
            }
            return n;
        }(c));
    }
    function o(e, t, n) {
        return "object" === (void 0 === e ? "undefined" : _typeof(e)) && (e[t] = n, !0);
    }
    function r(e, t) {
        var n = 24 * t * 60 * 60 * 1e3;
        return new Date(new Date(e).getTime() + n);
    }
    var i = {
        name: "booking",
        version: "1.4.1"
    }, a = {
        options: {
            checkInSelector: '[name="checkin"]',
            checkOutSelector: '[name="checkout"]',
            adultSelector: '[name="numofadult"]',
            childSelector: '[name="numofchild"]',
            roomSelector: '[name="numofroom"]',
            codeSelector: '[name="accesscode"]',
            submitSelector: '[name="search"]',
            propertyId: '[name="propertyId"]',
            onlineId: 4,
            builtinDatepicker: {
                enabled: !0,
                dateFormat: "dd M yy",
                altCheckin: "alt_checkin",
                altCheckout: "alt_checkout"
            },
            secretCode: null,
            propertyGroup: null,
            language: null,
            currency: null,
            confl: null,
            beforeSubmit: function() {},
            afterSubmit: function() {},
            debug: !1
        },
        init: function(t, o) {
            var c, l, u = t[i.name] = e.extend({
                element: t
            }, this.options, o), d = t, f = e(t);
            u.builtinDatepicker.enabled ? (!function(t) {
                var n = t[i.name], o = e(t), a = o.find(n.checkInSelector).attr("readonly", !0), c = o.find(n.checkOutSelector).attr("readonly", !0), l = e("<input/>").attr({
                    type: "hidden",
                    name: n.builtinDatepicker.altCheckin
                }), u = e("<input/>").attr({
                    type: "hidden",
                    name: n.builtinDatepicker.altCheckout
                });
                o.append(l), o.append(u);
                var d = new Date(), f = r(d, 1);
                a.datepicker({
                    minDate: d,
                    changeMonth: !1,
                    changeYear: !1,
                    dateFormat: n.builtinDatepicker.dateFormat,
                    altFormat: "yy-mm-dd",
                    altField: l,
                    numberOfMonths: 1,
                    onSelect: function(e, t) {
                        c.datepicker("option", "minDate", r(e, 1)), setTimeout(function() {
                            c.datepicker("show");
                        }, 350);
                    }
                }), c.datepicker({
                    minDate: f,
                    changeMonth: !1,
                    changeYear: !1,
                    dateFormat: n.builtinDatepicker.dateFormat,
                    altFormat: "yy-mm-dd",
                    altField: u,
                    numberOfMonths: 1
                }), a.datepicker("setDate", d), c.datepicker("setDate", f);
            }(d), c = f.find('[name="' + u.builtinDatepicker.altCheckin + '"]'), l = f.find('[name="' + u.builtinDatepicker.altCheckout + '"]')) : (c = f.find(u.checkInSelector), 
            l = f.find(u.checkOutSelector)), f.on("submit", function(e) {
                e.preventDefault(), u.beforeSubmit(d), n(d, c, l), u.afterSubmit(d);
            }), f.find(u.submitSelector).on("click", function(e) {
                e.preventDefault(), u.beforeSubmit(d), n(d, c, l), u.afterSubmit(d);
            }), u.debug && function(e, t) {
                e[i.name];
                var n = {
                    Element: e,
                    PluginName: i.name,
                    Version: i.version,
                    defaults: a.options,
                    configs: t
                };
                console.info(n);
            }(d, o);
        },
        update: function(e, t, n) {
            n[i.name][e] = t;
        }
    };
    e.fn[i.name] = function(t, n, o) {
        return "object" == (void 0 === t ? "undefined" : _typeof(t)) || void 0 == t ? this.each(function() {
            void 0 == this[i.name] && a.init(this, t);
        }) : "string" == typeof t && a[t] ? this.each(function() {
            a[t](n, o, this);
        }) : void e.error("No " + t + " method of $.fn.booking");
    };
}(jQuery);
//# sourceMappingURL=tl-booking.min.js.map