var {task, src, dest, watch, parallel, series} = require('gulp');
var sass = require('gulp-sass');
sass.compiler = require('dart-sass');
var postcss = require('gulp-postcss');
var postcssPresetEnv = require('postcss-preset-env');
var cssnano = require('cssnano');
var browser = require('browser-sync').create();

task('style', () => {
  return src('./assets/scss/main.scss', {sourcemaps:true})
    .pipe(sass.sync())
    .pipe(postcss([
      postcssPresetEnv(),
      cssnano(),
    ]))
    .pipe(dest('./assets/css', {sourcemaps:'.'}))
    .pipe(browser.stream())
})

task('server', () => {
  return browser.init({
    open: false,
    port: 3000,
    proxy: {
      target: 'localhost/burirasa.com',
    },
    ignore: [
      'node_modules/',
    ],
  })
});

var reload = done => {
  browser.reload();
  done();
}

task('watch', () => {
  watch([
    '**/*.php',
    '**/*.hmtl',
    '!node_modules/',
    'vendor/'
  ], series(reload));

  watch([
    './assets/scss/**/*.scss',
  ], parallel('style'));
});

task('build', parallel('style'));
task('serve', series('build', parallel('watch', 'server')));